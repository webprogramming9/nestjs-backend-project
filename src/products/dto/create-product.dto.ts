import { IsNotEmpty } from 'class-validator';
import { IsInt } from 'class-validator/types/decorator/decorators';

export class CreateProductDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsInt()
  price: number;
}
